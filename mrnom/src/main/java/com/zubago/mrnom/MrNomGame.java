package com.zubago.mrnom;


import com.zubago.mrnom.framework.Screen;
import com.zubago.mrnom.framework.impl.AndroidGame;

public class MrNomGame extends AndroidGame {
    public Screen getStartScreen() {
        return new LoadingScreen(this);
    }
}