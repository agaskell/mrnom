package com.zubago.mrnom;

import com.zubago.mrnom.framework.Game;
import com.zubago.mrnom.framework.Graphics;
import com.zubago.mrnom.framework.Input;
import com.zubago.mrnom.framework.Pixmap;
import com.zubago.mrnom.framework.Screen;

import java.util.List;

public class HelpScreen extends Screen {
    public HelpScreen(Game game) {
        super(game);
    }

    protected Screen getTransferScreen() {
        return new HelpScreen2(game);
    }

    protected Pixmap getContent() {
        return Assets.help1;
    }

    @Override
    public void update(float deltaTime) {
        List<Input.TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();
        for (Input.TouchEvent event : touchEvents) {
            if (event.type == Input.TouchEvent.TOUCH_UP) {
                if (event.x > 256 && event.y > 416) {
                    game.setScreen(getTransferScreen());
                    if (Settings.soundEnabled)
                        Assets.click.play(1);
                    return;
                }
            }
        }
    }

    @Override
    public void present(float deltaTime) {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.background, 0, 0);
        g.drawPixmap(getContent(), 64, 100);
        g.drawPixmap(Assets.buttons, 256, 416, 0, 64, 64, 64);
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
    }
}