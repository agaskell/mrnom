package com.zubago.mrnom.framework;

public interface Sound {
    public void play(float volume);

    public void dispose();
}
