package com.zubago.mrnom;

import com.zubago.mrnom.framework.Game;
import com.zubago.mrnom.framework.Pixmap;
import com.zubago.mrnom.framework.Screen;

public class HelpScreen2 extends HelpScreen {
    public HelpScreen2(Game game) {
        super(game);
    }

    @Override
    protected Screen getTransferScreen() {
        return new HelpScreen3(game);
    }

    @Override
    protected Pixmap getContent() {
        return Assets.help2;
    }

}
