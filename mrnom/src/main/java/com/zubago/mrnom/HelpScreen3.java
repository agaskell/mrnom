package com.zubago.mrnom;

import com.zubago.mrnom.framework.Game;
import com.zubago.mrnom.framework.Pixmap;
import com.zubago.mrnom.framework.Screen;

public class HelpScreen3 extends HelpScreen {
    public HelpScreen3(Game game) {
        super(game);
    }

    @Override
    protected Screen getTransferScreen() {
        return new MainMenuScreen(game);
    }

    protected Pixmap getContent() {
        return Assets.help3;
    }
}